import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsDefined, MinLength, IsString, IsNumber, Min } from 'class-validator';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  productId: number;

  @Column()
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(2, { always: true })
  name: string;

  @Column()
  @IsDefined({ always: true })
  @IsString({ always: true })
  @MinLength(3, { always: true })
  sku: string;

  @Column()
  @IsNumber()
  @Min(0, { always: true })
  price: string;
}
